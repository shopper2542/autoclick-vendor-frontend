import { React, useState, forwardRef, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import ac_logo from "../assets/ac_logo.png";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import DropZone1 from "./Dropzone1";
import Dialog from "@material-ui/core/Dialog";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import MaterialTable from "material-table";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    marginLeft: theme.spacing(2),
    flexGrow: 1,
  },
  appBar: {
    position: "relative"
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

// Main

export default function App() {
  // Handle Dialogs
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const classes = useStyles();

  // Info Constants

  const [companyName, setCompanyName] = useState("");
  const [address, setAddress] = useState("");
  const [vendorName, setVendorName] = useState("");
  const [companyTel, setCompanyTel] = useState("");
  const [vendorTel, setVendorTel] = useState("");
  const [email, setEmail] = useState("");
  const [lineId, setLineId] = useState("");
  const [ssNum, setSsNum] = useState("");
  const [companyType, setCompanyType] = useState("");
  const [companyAge, setCompanyAge] = useState("");
  const [fundAmount, setFundAmount] = useState("");
  const [url, setUrl] = useState([]);
  const [vendorInfo, setVendorInfo] = useState([]);
  const [upload, setUpload] = useState(false);

  // OnChange Info Constants (Collect Info.)

  const handleCompanyName = (e) => {
    setCompanyName(e.target.value);
  };

  const handleAddress = (e) => {
    setAddress(e.target.value);
  };

  const handleVendorName = (e) => {
    setVendorName(e.target.value);
  };

  const handleCompanyTel = (e) => {
    setCompanyTel(e.target.value);
  };

  const handleVendorTel = (e) => {
    setVendorTel(e.target.value);
  };

  const handleEmail = (e) => {
    setEmail(e.target.value);
  };

  const handleLineId = (e) => {
    setLineId(e.target.value);
  };

  const handleSsNum = (e) => {
    setSsNum(e.target.value);
  };

  const handleCompanyTypeChange = (e) => {
    setCompanyType(e.target.value);
  };

  const handleCompanyAge = (e) => {
    setCompanyAge(e.target.value);
  };

  const handleFundAmount = (e) => {
    setFundAmount(e.target.value);
  };

  // axios add info API

  async function addInfo() {
    let req = await axios({
      headers: { Authorization: "Bearer " + localStorage.getItem("TOKEN") },
      method: "post",
      url: `http://localhost:2542/vendor/info/add`,
      data: {
        vendor_company_name: companyName,
        vendor_address: address,
        vendor_name: vendorName,
        vendor_company_tel: companyTel,
        vendor_tel: vendorTel,
        vendor_email: email,
        vendor_line_id: lineId,
        vendor_ss_num: ssNum,
        vendor_business_type: companyType,
        vendor_business_age: companyAge,
        vendor_fund_amount: fundAmount,
        vendor_pdf_url: url,
      },
    });

    setUpload(true);
  }

  // axios add info API

  async function getInfo() {
    let req = await axios({
      headers: { Authorization: "Bearer " + localStorage.getItem("TOKEN") },
      method: "get",
      url: `http://localhost:2542/vendor/getVendorInfo`,
      data: {},
    });

    let res = await req;
    let resData = res.data;
    setVendorInfo(resData);
  }

  useEffect(() => {
    getInfo();
  }, [upload]);

  // handle Submit Button

  const handleSubmitButton = (event) => {
    addInfo();
    alert("ส่งข้อมูลเรียบร้อย");
    event.preventDefault();
  };

  // return of main

  return (
    <Grid container alignContent="center" justify="center">
      
      <Grid item xs={12}>
        <div className={classes.root}>
          <AppBar position="static" style={{ background: "#232831" }}>
            <Toolbar>
              <IconButton edge="start">
                <img
                  src={ac_logo}
                  alt="ac_logo"
                  height="50px"
                  width="120px"
                ></img>
              </IconButton>
              <Typography variant="h6" className={classes.title}></Typography>
              <Button color="inherit" onClick={handleClickOpen}>
                View All Submission
              </Button>
            </Toolbar>
          </AppBar>

          {/* View All Vendor Info Dialog */}
          <Dialog
            fullScreen
            open={open}
            onClose={handleClose}
            TransitionComponent={Transition}
          >
            <AppBar className={classes.appBar}>
              <Toolbar style={{ backgroundColor: "#232831" }}>
                <IconButton
                  edge="start"
                  color="inherit"
                  onClick={handleClose}
                  aria-label="close"
                >
                  <CloseIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  All Vendor Informations
                </Typography>
              </Toolbar>
            </AppBar>

            <Grid
              container
              alignContent="center"
              justify="center"
              style={{ padding: "20px" }}
            >
              <Grid item xs={12}>
                <MaterialTable
                  title=""
                  columns={[
                    { title: "ชื่อบริษัท", field: "vendor_company_name" },
                    { title: "ที่อยู่", field: "vendor_address" },
                    { title: "ชื่อผู้ติดต่อ", field: "vendor_name" },
                    {
                      title: "เบอร์โทรสำนักงาน",
                      field: "vendor_company_tel",
                      type: "numeric",
                    },
                    {
                      title: "เบอร์โทรมือถือ",
                      field: "vendor_tel",
                      type: "numeric",
                    },
                    { title: "Email", field: "vendor_email" },
                    { title: "LineID", field: "vendor_line_id" },
                    { title: "เลขประจำตัวผู้เสียภาษี", field: "vendor_ss_num" },
                    { title: "ประเภทธุรกิจ", field: "vendor_business_type" },
                    {
                      title: "ระยะเวลาในการทำธุรกิจ",
                      field: "vendor_business_age",
                      type: "numeric",
                      width: "50%",
                    },
                    {
                      title: "ทุนจดทะเบียน",
                      field: "vendor_fund_amount",
                      type: "numeric",
                    },
                    { title: "PDF URL", field: "vendor_pdf_url" },
                  ]}
                  data={vendorInfo}
                  options={{
                    rowStyle: {
                      backgroundColor: "#EEE",
                    },
                  }}
                />
              </Grid>
            </Grid>
          </Dialog>
        </div>
      </Grid>

      <Grid item xs={12}>
        <Card >
          <CardContent style={{ padding: "30px" }}>
            <Grid
              container
              spacing={3}
              alignContent="center"
              justify="center"
              style={{ padding: "30px" }}
            >
              <Grid item xs={10}>
                <Typography variant="h5" gutterBottom>
                  ส่วนที่ 1 : ข้อมูลทั่วไป
                </Typography>

                <TextField
                  label="ชื่อบริษัท: "
                  id="companyName"
                  variant="outlined"
                  fullWidth
                  onChange={handleCompanyName}
                />
              </Grid>

              <Grid item xs={10}>
                <TextField
                  label="ที่อยู่: "
                  id="address"
                  variant="outlined"
                  fullWidth
                  onChange={handleAddress}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="ชื่อผู้ติดต่อ: "
                  id="vendorName"
                  variant="outlined"
                  fullWidth
                  onChange={handleVendorName}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="เบอร์โทรสำนักงาน: "
                  id="companyTel"
                  variant="outlined"
                  fullWidth
                  onChange={handleCompanyTel}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="เบอร์มือถือ: "
                  id="vendorTel"
                  variant="outlined"
                  fullWidth
                  onChange={handleVendorTel}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="Email: "
                  id="email"
                  variant="outlined"
                  fullWidth
                  onChange={handleEmail}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="Line ID: "
                  id="lineId"
                  variant="outlined"
                  fullWidth
                  onChange={handleLineId}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="เลขประจำตัวผู้เสียภาษี: "
                  id="ssNum"
                  variant="outlined"
                  fullWidth
                  onChange={handleSsNum}
                />
              </Grid>
            </Grid>

            <Grid
              container
              spacing={3}
              alignContent="center"
              justify="center"
              style={{ padding: "30px", backgroundColor:"#eee" }}
            >
              <Grid item xs={10}>
                <Typography variant="h5" gutterBottom>
                  ส่วนที่ 2 : ประเภทธุรกิจ & ความสามารถในสินค้าและบริการ
                </Typography>

                <FormControl
                  variant="outlined"
                  className={classes.formControl}
                  fullWidth
                >
                  <InputLabel id="demo-simple-select-outlined-label">
                    ประเภทธุรกิจ
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={companyType}
                    onChange={handleCompanyTypeChange}
                    label="Age"
                  >
                    <MenuItem value="">
                      <em>เลือกประเภท</em>
                    </MenuItem>
                    <MenuItem value="Construction">Construction</MenuItem>
                    <MenuItem value="Building System">Building System</MenuItem>
                    <MenuItem value="Furniture">Furniture</MenuItem>
                    <MenuItem value="Computer Hardware & Software">
                      Computer Hardware & Software
                    </MenuItem>
                    <MenuItem value="Telecommunication">
                      Telecommunication
                    </MenuItem>
                    <MenuItem value="Electrical">Electrical</MenuItem>
                    <MenuItem value="Office Suppliers">
                      Office Suppliers
                    </MenuItem>
                    <MenuItem value="Tools & Equipment">
                      Tools & Equipment
                    </MenuItem>
                    <MenuItem value="Uniform">Uniform</MenuItem>
                    <MenuItem value="Gift & Premium">Gift & Premium</MenuItem>
                    <MenuItem value="Car">Car</MenuItem>
                    <MenuItem value="Car">Printing</MenuItem>
                    <MenuItem value="Cleaning Supplies">
                      Cleaning Supplies
                    </MenuItem>
                    <MenuItem value="Security & Safety">
                      Security & Safety
                    </MenuItem>
                    <MenuItem value="Food & Beverages">
                      Food & Beverages
                    </MenuItem>
                    <MenuItem value="Landscape">Landscape</MenuItem>
                    <MenuItem value="Others">Others</MenuItem>
                    {console.log(companyType)}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="ระยะเวลาในการทำธุรกิจ: "
                  id="companyAge"
                  variant="outlined"
                  fullWidth
                  onChange={handleCompanyAge}
                />
              </Grid>

              <Grid item xs={5}>
                <TextField
                  label="ทุนจดทะเบียน: "
                  id="fundAmount"
                  variant="outlined"
                  fullWidth
                  onChange={handleFundAmount}
                />
              </Grid>
            </Grid>

            <Grid
              container
              spacing={3}
              alignContent="center"
              justify="center"
              style={{ padding: "30px" }}
            >
              <Grid item xs={10}>
                <Typography variant="h5" gutterBottom>
                  ส่วนที่ 3 : เอกสารแนบ
                </Typography>
              </Grid>

              <Grid item xs={5}>
                <Typography variant={"h5"}>กรณีบุคคลธรรมดา</Typography>

                <Typography>1. สำเนาบัตรประชาชน</Typography>
                <Typography>2. ผลงานย้อนหลัง</Typography>
                <Typography>
                  <b>**อัพโหลดไฟล์แนบเป็นไฟล์ .pdf ขนาดไฟล์ไม่เกิน 5MB.**</b>
                </Typography>
              </Grid>

              <Grid item xs={5}>
                <Typography variant={"h5"}>กรณีนิติบุคคล</Typography>

                <Typography>1. สำเนาหนังสือรับรองบริษัท / หจก.</Typography>
                <Typography>2. สำเนาใบทะเบียนภาษีมูลค่าเพิ่ม ภ.พ.20</Typography>
                <Typography>
                  3. โปรไฟล์บริษัท (Company Profile) / ผลงานย้อนหลัง
                </Typography>
                <Typography>
                  <b>**อัพโหลดไฟล์แนบเป็นไฟล์ .pdf ขนาดไฟล์ไม่เกิน 5MB.**</b>
                </Typography>
              </Grid>

              <Grid item xs={12}>
                <DropZone1 setInfoUrl={(url) => setUrl(url)} />
                <Typography>{url}</Typography>
              </Grid>
            </Grid>
            <Grid container alignContent="center" justify="center">
              <Grid item>
                <Button
                  variant="contained"
                  size="large"
                  color="primary"
                  onClick={handleSubmitButton}
                  className={classes.margin}
                >
                  <Typography variant="h5">ส่งข้อมูล</Typography>
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}
