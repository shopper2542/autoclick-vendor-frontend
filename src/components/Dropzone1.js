import { React, useState, useMemo } from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import fileIcon from "../assets/fileIcon.png";
import axios from "axios";
import { useDropzone } from "react-dropzone";

const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

function Dropzone(props) {
  const [pdfUrl, setPdfUrl] = useState([]);

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
    acceptedFiles,
  } = useDropzone({ accept: "application/pdf" });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragAccept, isDragReject]
  );

  const files = acceptedFiles.map((file) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  let file = new FormData();

  const fileObjects = acceptedFiles.map((files) => {
    console.log(file);
    file.append("file", files, files.name);
  });

  async function getPdfUrl() {
    let req = await axios({
      headers: { Authorization: "Bearer " + localStorage.getItem("TOKEN") },
      method: "post",
      url: `http://localhost:2542/upload`,
      data: file,
    });

    let res = await req;
    let resData = res.data;
    //console.log(resData.Location);
    setPdfUrl(resData.Location);
    console.log(resData.Location);
  }

  const handleSubmit = () => {
    getPdfUrl();
    props.setInfoUrl(pdfUrl);
  };

  return (
    <div className="container">
      <div {...getRootProps({ style })}>
        <input {...getInputProps()} />
        <img width="10%" height="5%" src={fileIcon} alt="fileIcon"></img>
        <p>ลากและวางไฟล์ไว้ที่นี่ หรือ คลิ๊กเพื่อเลือกไฟล์</p>
        <aside>
          <h4>Files</h4>
          <ul>{files}</ul>
        </aside>
      </div>
      <Grid container alignContent="center" justify="center" style={{ padding: "20px" }}>
        <Grid item>
          <Button variant="outlined" onClick={handleSubmit}>
            Submit Pdf
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}

export default Dropzone;
